Momentum
========

Momentum is a simple tasks scheduling library allowing long running processes
by handling `millis()`'s overflow.

Tasks can be scheduled up to 49 days[^1] in advance with an accuracy of one
millisecond.

[^1]: To be more precise: 2^(32)-1 milliseconds, or 49 days, 17
      hours, 2 minutes, 47 secondes and 295 milliseconds.

Install
-------

Momentum is compatible with Arduino Software >= 1.5.

Copy the `Momentum` folder into your libraries folder. Under Windows, it will
likely be called `My Documents\Arduino\libraries`. For Mac users, it will
likely be called `Documents/Arduino/libraries`. On Linux, it will be the
`libraries` folder in your sketchbook.

Usage
-----

This section is a quick introduction to the Momentum library and its main class
`Task`. Working examples are available in the `examples` folder.

### Task creation

Tasks can be created *scheduled*

```c++
Task myTask = Task(Second(10));
```

or *unscheduled*

```c++
Task myTask = Task();
```

### Task scheduling

The main way to schedule a task is to call the `Task.schedule()` method
which takes an interval in milliseconds as argument.

To avoid tedious conversions, Momentum provides appropriate macros:

* `Days()`
* `Hours()`
* `Minutes()`
* `Seconds()`
* `Millis()`[^2]

which return the corresponding milliseconds value and can be added among them.

[^2]: The `Millis()` macro is provided for the sake of uniformity.

So, to schedule a task 2 hours and 15 minutes in advance, simply write:

```c++
myTask.schedule(Hours(2) + Minutes(15));
```

A task can be scheduled to occur as soon as possible either by calling
`Task.schedule()` with `0` as argument or by calling the `Task.asap()`
method.

### Task execution

The standard pattern of execution of a task is to check if it's time for it
to occur with the `Task.ready()` method.

Once the task is done, you **MUST** mark it as completed by calling the
`Task.done()` method **OR** schedule it again (e.g. recurring task).

```c++
if (myTask.ready()) {
  // task's code

  myTask.done();
}
```

The `Task.done()` method can also be used to disable a task before it occur.

Licensing
---------

This library is published under the terms of the MIT License.
For furter details, please see the file called `LICENSE`.
