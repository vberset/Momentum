/*
  Blink
  Turns on an LED on for 500 millisecond, then off for one second, repeatedly,
  using Momentum library.
 */

// include Momentum library
#include <Momentum.h>

// define the pin number of the LED
int ledPin = 13;

// store the current LED's state, initialized to be off
int ledState = LOW;

// instantiate a Task object used to schedule LED's state toggling
Task nextToggle;

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital LED's pin as an output
  pinMode(ledPin, OUTPUT);
  
  // enforce initial LED's state
  digitalWrite(ledPin, ledState);
  
  // schedule the first toggling to occur as soon as possible
  nextToggle.asap();
}

// the loop function runs over and over again forever
void loop() {
  // check if it is time to toggle the LED's state
  if (nextToggle.ready()) {
    // if the LED is on, set it off for 1 second
    if (ledState == HIGH) {
      ledState = LOW;
      digitalWrite(ledPin, ledState);
      nextToggle.schedule(Seconds(1));
    // else, set it on for 500 milliseconds
    } else {
      ledState = HIGH;
      digitalWrite(ledPin, ledState);
      nextToggle.schedule(Millis(500));
    }
  }
}
