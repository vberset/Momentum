/*
 * Momentum.cpp - Simple tasks scheduling library
 *
 * Copyright (c) 2015 Vincent Berset <vberset@protonmail.ch>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#include "Momentum.h"

#define MILLIS_MAX 0xFFFFFFFF

const unsigned long Task::SECOND = 1000;
const unsigned long Task::MINUTE = Seconds(60);
const unsigned long Task::HOUR   = Minutes(60);
const unsigned long Task::DAY    = Hours(24);

Task::Task ()
{
  scheduled = false;
}

Task::Task (unsigned long interval)
{
  schedule(interval);
}

void Task::asap ()
{
  origin = momentum = millis();
  scheduled = true;
}

void Task::schedule (unsigned long interval)
{
  scheduled = true;
  origin = millis();
  momentum = origin + interval;
}

void Task::done ()
{
  scheduled = false;
}

bool Task::ready ()
{
  unsigned long now = millis();
  return (scheduled &&
          (((origin <= momentum) && (momentum <= now)) ||
           ((now < origin) && (origin <= momentum)) ||
           ((momentum <= now) && (now < origin))));
}

void Task::join ()
{
  unsigned long now = millis();

  if (!scheduled || ready())
    return;

  if (now < momentum)
    delay(momentum - now);
  else
    delay(momentum + (MILLIS_MAX - now));
}

bool Task::is_scheduled ()
{
  return scheduled;
}
