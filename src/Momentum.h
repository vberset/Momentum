/*
 * Momentum.h - Simple tasks scheduling library
 *
 * Copyright (c) 2015 Vincent Berset <vberset@protonmail.ch>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
#ifndef Momentum_H
#define Momentum_H

#include "Arduino.h"

#define Millis(x)  (x)
#define Seconds(x) (x * Task::SECOND)
#define Minutes(x) (x * Task::MINUTE)
#define Hours(x)   (x * Task::HOUR)
#define Days(x)    (x * Task::DAY)

class Task {
  public:
    static const unsigned long SECOND;
    static const unsigned long MINUTE;
    static const unsigned long HOUR;
    static const unsigned long DAY;

    /**
     * Create a unscheduled task.
     */
    Task ();

    /**
     * Create a scheduled task.
     */
    Task (unsigned long);

    /**
     * Schedule the task to occur within the specified time.
     */
    void schedule (unsigned long);

    /**
     * Schedule the task to occur as soon as possible.
     * Thus, it can occur in less than a millisecond.
     */
    void asap ();

    /**
     * Set the task to never occur (eg. disable the task).
     */
    void done ();

    /**
     * Tests whether or not the task must occur.
     */
    bool ready ();

    /**
     * Wait with the task until it must occur.
     */
    void join ();

    /**
     * Return true if the task is scheduled.
     */
    bool is_scheduled ();

  private:
    bool scheduled;
    unsigned long origin;
    unsigned long momentum;
};
#endif
